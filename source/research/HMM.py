
from collections import namedtuple, defaultdict
from math import ceil, log
from io import StringIO
from randomization_tools import get_weighted_list
from random import choice, randint
from copy import deepcopy
from transducer import Transducer, Arc, CostVector, State
from grammar.feature_table import FeatureTable, MORPHEME_BOUNDARY, Segment, NULL_SEGMENT, JOKER_SEGMENT


import logging



from otml_configuration_manager import OtmlConfigurationManager, OtmlConfigurationError


logger = logging.getLogger(__name__)
configurations = OtmlConfigurationManager.get_instance()
if configurations is None:
    raise OtmlConfigurationError("OtmlConfigurationManager was not initialized")

INITIAL_STATE = 'q0'
FINAL_STATE = 'qf'


ALLOW_DUPLICATE_EMISSIONS = True #what does this mean
REMOVE_ORIG = True



StateTuple = namedtuple('StateTuple', ['start_state', 'end_state'])
class HMM:
    def __init__(self, hmm_dict, feature_table):
        initial_state_transitions = hmm_dict.pop(INITIAL_STATE)
        self.transitions = {k: v[0] for (k, v) in hmm_dict.items()}  #first value in dict is a target state q1q2...
        self.transitions[INITIAL_STATE] = initial_state_transitions
        self.emissions = {k: v[1] for (k, v) in hmm_dict.items()} #second value in dict is an emission q1 'kat'
        self.inner_states = self._get_inner_states()
        self.feature_table = feature_table
        self.list_of_transducer_states=list()



    @classmethod
    def create_hmm_from_dicts(cls, transition_dict, emission_dict):
        hmm = HMM({INITIAL_STATE: [FINAL_STATE]})
        hmm.transitions = {k: v for k, v in transition_dict}
        hmm.emissions = {k: v for k, v in emission_dict}
        hmm.inner_states = hmm._get_inner_states()
        return hmm

    def get_transducer(self):
        list_of_transducer_states = []
        HMM_INITIAL_STATE = INITIAL_STATE  # init state of hmm is the start node of FST
        HMM_FINAL_STATE = FINAL_STATE
        segments = self.feature_table.get_segments()
        transducer = Transducer(segments, name="HMM", length_of_cost_vectors=0)
        list_of_transducer_states.append(HMM_INITIAL_STATE)
        #transducer.initial_state = HMM_INITIAL_STATE

        state_dict = dict()
        state_dict[HMM_INITIAL_STATE] = StateTuple(None, HMM_INITIAL_STATE)


        for hmm_state in sorted(self.inner_states):  # sorted for consistency in encoding later on
            start_state = "{}_start".format(hmm_state)
            end_state = "{}_end".format(hmm_state)
            list_of_transducer_states.extend([start_state, end_state])
            #transducer.states.extend([start_state, end_state])
            state_dict[hmm_state] = StateTuple(start_state, end_state)
        for hmm_state in self.emissions:
            for emission_index, emission in enumerate(self.emissions[hmm_state]):
                if configurations["MORPHEME_BOUNDARY_FLAG"] or configurations["WORD_BOUNDARY_FLAG"]:
                    if configurations["MORPHEME_BOUNDARY_FLAG"]:
                        emission_states_list = ["{},{},{}".format(hmm_state,emission_index, segment_index) for segment_index in range(len(emission))]
                    else:
                        emission_states_list = ["{},{},{}".format(hmm_state,emission_index, segment_index)for segment_index in range(len(emission)-1)]
                else:
                    emission_states_list=["{},{},{}".format(hmm_state,emission_index, segment_index)for segment_index in range(len(emission)-1)]


                list_of_transducer_states.extend(emission_states_list)
                #transducer.states.extend(emission_states_list)

        list_of_transducer_states.append(HMM_FINAL_STATE)
        #transducer.add_final_state(HMM_FINAL_STATE)


        initial_state = list_of_transducer_states.index(HMM_INITIAL_STATE)
        final_state = list_of_transducer_states.index(HMM_FINAL_STATE)



        # transiton arcs
        for hmm_state1 in self.transitions:
            for hmm_state2 in self.transitions[hmm_state1]:
                state1 = list_of_transducer_states.index(state_dict[hmm_state1].end_state)
                if hmm_state2 != HMM_FINAL_STATE:
                    state2 = list_of_transducer_states.index(state_dict[hmm_state2].start_state)
                    transducer.add_arc(Arc(state1, Segment("Epsilon"), Segment("Epsilon"), CostVector.get_empty_vector(), state2))
                else:
                    if configurations["WORD_BOUNDARY_FLAG"]:
                        transducer.add_arc(Arc(state1, Segment("WB"), Segment("WB"),
                                               CostVector.get_empty_vector(), final_state))
                    else:
                        transducer.add_arc(Arc (state1, Segment("Epsilon"), Segment("Epsilon"),
                                               CostVector.get_empty_vector(), final_state))




        # emission arcs

        for hmm_state in self.emissions:
            start_state = state_dict[hmm_state].start_state
            end_state = state_dict[hmm_state].end_state
            for emission_index, emission in enumerate(self.emissions[hmm_state]):
                if not configurations["MORPHEME_BOUNDARY_FLAG"]:
                    string_states_list = [start_state] + ["{},{},{}".format(hmm_state, emission_index, segment_index) for segment_index in range(len(emission) - 1)] + [end_state]
                    states_list = [list_of_transducer_states.index(state) for state in string_states_list]

                    for i in range(len(emission)):
                        emission_segment = Segment(emission[i])
                        for seg in segments:
                            if seg == emission_segment:
                                transducer.add_arc(Arc(states_list[i], seg, JOKER_SEGMENT, CostVector.get_empty_vector(), states_list[i + 1]))
                else:
                    string_states_list = [start_state] + ["{},{},{}".format(hmm_state, emission_index, segment_index)
                                                          for segment_index in range(len(emission))] + [end_state]
                    states_list = [self.list_of_transducer_states.index(state) for state in string_states_list]

                    for i in range(len(emission)):
                        transducer.add_arc(Arc(states_list[i], emission[i], emission[i], CostVector.get_empty_vector(),
                                               states_list[i + 1]))
                    i += 1
                    transducer.add_arc(Arc(states_list[i], MORPHEME_BOUNDARY, MORPHEME_BOUNDARY, CostVector.get_empty_vector(), states_list[i + 1]))


        return transducer

    def _get_inner_states(self):
        states = set()
        for state_key in self.transitions:
            states.add(state_key)
            for state_transition in self.transitions[state_key]:
                states.add(state_transition)
        if FINAL_STATE in states:
            states.remove(FINAL_STATE)
        if INITIAL_STATE in states:
            states.remove(INITIAL_STATE)
        return list(states)

    def get_states(self):
        return [INITIAL_STATE] + self.inner_states + [FINAL_STATE]

    def get_transitions(self, state):
        """used in get_encoding_length"""
        return self.transitions.get(state, [])

    def get_emissions(self, state):
        """used in get_encoding_length"""
        return self.emissions.get(state, [])

    def get_all_emissions(self):
        emissions = set()
        for state in self.emissions:
            emissions |= set(self.emissions[state])
        return list(emissions)

    def get_encoding_length(self, segment_symbol_length):
        states_list = self.get_states()
        states_symbol_length = ceil(log(len(states_list) + 1, 2))  # + 1 for the delimiter

        state_symbols_in_transitions = 0
        total_num_of_emissions = 0
        segments_in_emissions = 0

        for state in states_list:
            state_symbols_in_transitions += len(self.get_transitions(state)) + 1  # +1 indicate the origin state

            for emission in self.get_emissions(state):
                total_num_of_emissions += 1
                segments_in_emissions += len(emission)

        num_bits = states_symbol_length + 1
        content_usage = (state_symbols_in_transitions * states_symbol_length) + (
        segments_in_emissions * segment_symbol_length)
        delimiter_usage = (len(states_list) * segment_symbol_length) + \
                          (len(states_list) * states_symbol_length) + \
                          total_num_of_emissions * segment_symbol_length
        encoding_length = num_bits + content_usage + delimiter_usage

        return encoding_length

    def _get_next_state(self):
        state_numbers = [int(x[1:]) for x in self.inner_states]  # remove 'q'
        state_numbers.append(0)
        state_numbers.sort()
        next_state_number = None

        if len(state_numbers) == state_numbers[-1] + 1:
            next_state_number = len(state_numbers)
        else:
            for i in range(len(state_numbers) - 1):
                if state_numbers[i] + 1 != state_numbers[i + 1]:
                    next_state_number = i + 1
                    break

        new_state = 'q{}'.format(next_state_number)

        return new_state

    def draw(self):
        def weight_str(dict_, key, value):
            weight = 1
            if weight != 1:
                return "({})".format(weight)
            else:
                return ""

        str_io = StringIO()
        print("digraph acceptor {", file=str_io, end="\n")
        print("rankdir=LR", file=str_io, end="\n")
        print("size=\"11,5\"", file=str_io, end="\n")
        print("node [shape = ellipse];", file=str_io, end="\n")

        print("//transitions arcs", file=str_io, end="\n")
        for state1 in self.transitions:
            for state2 in self.transitions[state1]:
                print("\"{}\" ->  \"{}\" [label=\"{}\"];".format(state1, state2,
                                                                 weight_str(self.transitions, state1, state2)),
                      file=str_io, end="\n")

        print("//emission tables lines", file=str_io, end="\n")
        for state in self.emissions:
            print("\"{}\" -- {}_emission_table".format(state, state), file=str_io, end="\n")

        print("//emission tables", file=str_io, end="\n")
        for state in self.inner_states:
            emissions_label = ""
            for emission in self.emissions[state]:
                emissions_label += "{} {}\\n".format(emission, weight_str(self.emissions, state, emission))
            print("{}_emission_table [shape=none, label=\"{}\"]".format(state, emissions_label), file=str_io, end="\n")

        print("\"qH0\" [style=filled];", file=str_io, end="\n")
        print("\"qHf\" [peripheries=2];", file=str_io, end="\n")
        print("}", file=str_io, end="")

        return str_io.getvalue()


# mutations - all mutations return true or false depending on there success
    def make_mutation(self):

        segments = [segment.get_symbol() for segment in self.feature_table.get_segments()]
        #if configurations["MORPHEME_BOUNDARY_FLAG"]:  related to get transducer symbol table in ESP - check
           # segments.remove(MORPHEME_BOUNDARY)

        mutation_weights = [
            ([self.combine_emissions, []], configurations["COMBINE_EMISSIONS"]),
            ([self.advance_emission, []], configurations["ADVANCE_EMISSION"]),
            ([self.clone_state, []], configurations["CLONE_STATE"]),
            ([self.clone_emission, []], configurations["CLONE_EMISSION"]),
            ([self.add_state, []], configurations["ADD_STATE"]),
            ([self.remove_state, []], configurations["REMOVE_STATE"]),
            ([self.add_transition, []], configurations["ADD_TRANSITION"]),
            ([self.remove_transition, []], configurations["REMOVE_TRANSITION"]),
            ([self.add_segment_to_emission, [segments]], configurations["ADD_SEGMENT_TO_EMISSION"]),
            ([self.remove_segment_from_emission, []], configurations["REMOVE_SEGMENT_FROM_EMISSION"]),
            ([self.change_segment_in_emission, [segments]], configurations["CHANGE_SEGMENT_IN_EMISSION"]),
            ([self.add_emission_to_state, [segments]], configurations["ADD_EMISSION_TO_STATE"]),
            ([self.remove_emission_from_state, []], configurations["REMOVE_EMISSION_FROM_STATE"]),
        ]

        if configurations["UNDERSPECIFICATION_FLAG"]:
            mutation_weights.append(([self.underspecify, []], configurations["UNDERSPECIFY"]))

        mutations_tuple_list = get_weighted_list(mutation_weights)
        mutations_tuple = choice(mutations_tuple_list)
        print("mutation:", mutations_tuple) # TODO: make it pretty
        mutation_result = mutations_tuple[0](*mutations_tuple[1])

        return mutation_result

    def advance_emission(self):
        target_state = choice(self.inner_states)
        target_state_emissions = self.get_emissions(target_state)
        if target_state in self.transitions[target_state] and len(self.transitions[target_state]) > 1 \
                    and len(target_state_emissions) > 1 and len(self.inner_states) < configurations[
                "MAX_NUM_OF_INNER_STATES"]:
            # check if there is a loop and another outgoing state
            outgoing_states_list = list(set(self.transitions[target_state]) - set([target_state]))
            outgoing_state = choice(outgoing_states_list)
            new_state = self._get_next_state()
            emission = choice(target_state_emissions)

            self.inner_states.append(new_state)
            self.transitions[new_state] = [outgoing_state, new_state, target_state]
            self.emissions[new_state] = [emission]

            self.transitions[target_state].append(new_state)
            self.emissions[target_state].remove(emission)
            return True
        else:
            return False

    def combine_emissions(self):
        """pick two emissions - combine them and add them to random state"""
        emissions = self.get_all_emissions()
        emission1 = choice(emissions)
        emission2 = choice(emissions)
        new_emission = emission1 + emission2
        state = choice(self.inner_states)
        if new_emission not in self.get_emissions(state):
            self.emissions[state].append(new_emission)
            return True
        else:
            return False

    def clone_state(self):
        """pick an inner state, create a new state with same transitions and emissions,
        if the original state a transition to itself the original and the new will be connected"""
        if len(self.inner_states) < configurations["MAX_NUM_OF_INNER_STATES"]:
            original_state = choice(self.inner_states)
            cloned_state = self._get_next_state()
            self.inner_states.append(cloned_state)
            self.emissions[cloned_state] = deepcopy(self.emissions[original_state])

            # create incoming connections
            for state in self.transitions:
                if original_state in self.transitions[state]:
                    self.transitions[state].append(cloned_state)

            # copy outgoing connections
            self.transitions[cloned_state] = deepcopy(self.transitions[original_state])

            return True
        else:
            return False

    def clone_emission(self):
        """pick an emission
           pick an inner state and add the emission to it"""
        emissions = self.get_all_emissions()
        emission = choice(emissions)
        state = choice(self.inner_states)
        if emission not in self.get_emissions(state):
            self.emissions[state].append(emission)
            return True
        else:
            return False

    def add_state(self):
        """adds empty state"""
        if len(self.inner_states) < configurations["MAX_NUM_OF_INNER_STATES"]:
            new_state = self._get_next_state()
            self.inner_states.append(new_state)
            self.emissions[new_state] = []
            self.transitions[new_state] = []
            return True
        else:
            return False

    def remove_state(self):
        """removes an inner state (and all it's arcs)"""
        if len(self.inner_states) > configurations["MIN_NUM_OF_INNER_STATES"]:
            state_to_remove = choice(self.inner_states)
            self.inner_states.remove(state_to_remove)
            del self.emissions[state_to_remove]
            del self.transitions[state_to_remove]

            for state in self.transitions:
                if state_to_remove in self.transitions[state]:
                    self.transitions[state].remove(state_to_remove)
            return True
        else:
            return False

    def add_transition(self):
        """picks a state form initial+inner states and add transition
        to other random state (in case initial, not to itself)"""
        state1 = choice(self.inner_states + [INITIAL_STATE])
        state2 = choice(self.inner_states)
        if state2 not in self.get_transitions(state1):
            self.transitions[state1].append(state2)
            return True
        else:
            return False

    def remove_transition(self):
        """picks a state form initial+inner states and remove a transition from it"""
        state1 = choice(self.inner_states + [INITIAL_STATE])
        if not len(self.get_transitions(state1)):
            return False
        else:
            state2 = choice(self.get_transitions(state1))
            self.transitions[state1].remove(state2)
            return True

    def add_segment_to_emission(self, segments):
        """pick an inner state, pick an emission, pick a segment and insert in random position"""
        state = choice(self.inner_states)
        if self.get_emissions(state):
            emission = choice(self.get_emissions(state))
            segment = choice(segments)

            insertion_index = randint(0, len(emission))
            new_emission = emission[:insertion_index] + segment + emission[insertion_index:]
            if ALLOW_DUPLICATE_EMISSIONS or new_emission not in self.get_emissions(state):
                self.emissions[state].append(new_emission)
                if REMOVE_ORIG:
                    self.emissions[state].remove(emission)
                return True

        return False

    def remove_segment_from_emission(self):
        """pick an inner state, pick an emission, pick a position and remove from it -
        if the emission is mono-segmental, remove it"""

        state = choice(self.inner_states)
        emissions = self.get_emissions(state)
        if emissions:
            emission = choice(emissions)
            if not len(emission) == 1:
                deletion_index = randint(0, len(emission) - 1)
                new_emission = emission[:deletion_index] + emission[deletion_index + 1:]
                if ALLOW_DUPLICATE_EMISSIONS or new_emission not in self.get_emissions(state):
                    self.emissions[state].append(new_emission)
            self.emissions[state].remove(emission)
            return True

        return False

    def change_segment_in_emission(self, segments):
        """pick a state, pick an emission, pick a segment, change it"""
        state = choice(self.inner_states)
        emissions = self.get_emissions(state)
        if emissions:
            emission = choice(emissions)
            # crate new emission
            emission_string_list = list(emission)
            index_of_change = randint(0, len(emission_string_list) - 1)
            old_segment = emission_string_list[index_of_change]
            segments.remove(old_segment)
            new_segment = choice(segments)
            emission_string_list[index_of_change] = new_segment
            new_emission = ''.join(emission_string_list)

            # replace emission
            emissions.remove(emission)
            emissions.append(new_emission)
            return True
        else:
            return False

    def add_emission_to_state(self, segments):
        """pick an inner state, pick a segment - add segment to state """
        state = choice(self.inner_states)
        segment = choice(segments)
        if ALLOW_DUPLICATE_EMISSIONS or segment not in self.get_emissions(state):
            self.emissions[state].append(segment)
            return True
        else:
            return False

    def remove_emission_from_state(self):
        """pick an inner state, pick an emission - remove it (if state has no emissions - mutation failed)"""
        state = choice(self.inner_states)
        emissions = self.get_emissions(state)
        if emissions:
            emission = choice(emissions)
            self.emissions[state].remove(emission)
            return True
        else:
            return False

    def underspecify(self):
        """pick a state, pick an emission, pick a voiced segment - underspecify"""
        state = choice(self.inner_states)
        emissions = self.get_emissions(state)
        if emissions:
            emission = choice(emissions)
            if "t" in emission or "d" in emission:
                # crate new emission
                emission_string_list = list(emission)
                indices = [i for i, segment in enumerate(emission_string_list) if segment == "t" or segment == "d"]
                index_of_change = choice(indices)
                new_segment = "T"
                emission_string_list[index_of_change] = new_segment
                new_emission = ''.join(emission_string_list)

                # replace emission
                emissions.remove(emission)
                emissions.append(new_emission)
                return True

        return False

        # emission generation

    def generate_emission(self):
        """this will only work with well structured hmm (all inner states with emissions, no dead ends)"""
        result = ""
        current_state = choice(self.transitions[INITIAL_STATE])
        while current_state != FINAL_STATE:
            emission = choice(self.emissions[current_state])
            result += emission
            current_state = choice(self.transitions[current_state])
        return result

    def generate_emissions_list(self, n):
        result = []
        for _ in range(n):
            result.append(self.generate_emission())
        return result



    def get_log_lines(self):
        log_lines = list()
        log_lines.append("HMM:")
        log_lines.append("{}: {}".format(INITIAL_STATE, sorted(self.transitions[INITIAL_STATE])))

        for state in sorted(self.inner_states):
            emissions = sorted(self.emissions[state])
            transitions = sorted(self.transitions[state])
            log_lines.append("{}: {}, {}".format(state, sorted(transitions), sorted(emissions)))

        for path in self.get_all_paths():
            log_lines.append("->".join(path))
        # log_lines.append("All Emissions: {}".format(sorted(self.get_all_emissions())))

        return log_lines

    def get_all_paths(self):
        paths = []
        stack = [(INITIAL_STATE, [INITIAL_STATE])]
        while stack:
            (vertex, path) = stack.pop()
            for next in set(self.transitions[vertex]) - set(path):
                if next == FINAL_STATE:
                    paths.append(path + [next])
                else:
                    stack.append((next, path + [next]))
        return paths

    def _get_number_of_segments(self):
        return sum([len(i) for k, v in self.emissions.items() for i in v])
    def get_list_of_transducer_states(self):
        return self.list_of_transducer_states
    def __str__(self):
        return "HMM, number of words {}, number of segments {},  emissions {}, states {}, transitions {}, ".format(len(self.get_all_emissions()), self._get_number_of_segments(), self.emissions, self.inner_states, self.transitions)


from tests.otml_configuration_for_testing import configurations
from debug_tools import write_to_dot
from grammar.feature_table import FeatureTable
from research.HMM import HMM, INITIAL_STATE, FINAL_STATE
from tests.persistence_tools import get_feature_table_fixture
from tests.stochastic_testcase import StochasticTestCase


class TestHMM(StochasticTestCase):
    #feature_table = FeatureTable.load(get_feature_table_fixture("plural_english_feature_table.json"))
    feature_table = FeatureTable.load(get_feature_table_fixture("french_deletion_feature_table.json"))
    def test_plural_english(self):
        self.feature_table = FeatureTable.load(get_feature_table_fixture("plural_english_feature_table.json"))
        hmm = HMM({INITIAL_STATE: ['q1'],
                     'q1': (['q2', FINAL_STATE], ['dog', 'kat']),
                     'q2': ([FINAL_STATE], ['z'])}, self.feature_table)

        print('\nINITIAL HMM',hmm)
        print('\nENCODING LENGTH', hmm.get_encoding_length(4))

    def test_get_inner_states(self):
        print('\nGET INNER STATES')
        hmm = HMM({INITIAL_STATE: ['q1'],
                   'q1': (['q2', FINAL_STATE], ['dog', 'kat']),
                   'q2': ([FINAL_STATE], ['z'])}, self.feature_table)

        print(hmm._get_inner_states())

    def test_advance_emission(self):
        print ('ADVANCE EMISSION')
        hmm = HMM({'qH0': ['q1'],
                   'q1': (['q1', 'qHf'], ['dag', 'kat', 'dot', 'kod', 'gas', 'toz'] + ['zo', 'go', 'do'] + ['at'])
                   },self.feature_table)
        print('before advance \n')
        for line in hmm.get_log_lines():
            print(line)

        hmm.advance_emission()
        print('\nafter advance \n')
        for line in hmm.get_log_lines():
            print(line)



    def test_get_log_lines(self):
        print ('\nGET LOG LINES')
        hmm = HMM({'qH0': ['q1'],
                   'q1': (['q2', 'q3', 'qHf'], ['dag', 'kat', 'dot', 'kod', 'gas', 'toz']),
                   'q2': (['q3', 'qHf'], ['zo', 'go', 'do']),
                   'q3': (['qHf'], ['as', 'ak', 'at'])},self.feature_table)
        for line in hmm.get_log_lines():
            print(line)

    def test_get_all_paths(self):
        print ('\nGET ALL PATHS')
        hmm = HMM({'qH0': ['q1'],
                   'q1': (['q2', 'q3', 'qHf'], []),
                   'q2': (['q3', 'qHf'], []),
                   'q3': (['qHf'], [])}, self.feature_table)
        print(list(hmm.get_all_paths()))

    def test_change_segment_in_emission(self):
        print ('\nCHANGE SEGMENT IN EMISSION')

        hmm = HMM({INITIAL_STATE: ['q1'],
                   'q1': (['q2', FINAL_STATE], ['dog', 'kat']),
                   'q2': ([FINAL_STATE], ['z'])}, self.feature_table)

        segments = [segment.get_symbol() for segment in self.feature_table.get_segments()]
        hmm.change_segment_in_emission(segments)
        print(hmm.get_all_emissions())



    def test_get_states(self):
        print('\nGET STATES')
        hmm = HMM({INITIAL_STATE: ['q1'],
                   'q1': (['q2', FINAL_STATE], ['dog', 'kat']),
                   'q2': ([FINAL_STATE], ['z'])}, self.feature_table)

        print(hmm.get_states())

    def test_get_transitions(self):

        print('\nGET TRANSITIONS')
        hmm = HMM({INITIAL_STATE: ['q1'],
                   'q1': (['q2', FINAL_STATE], ['dog', 'kat']),
                   'q2': ([FINAL_STATE], ['z'])}, self.feature_table)

        print(hmm.get_transitions('q1'))

    def test_get_emissions(self):

        print('\nGET EMISSIONS')
        hmm = HMM({INITIAL_STATE: ['q1'],
                   'q1': (['q2', FINAL_STATE], ['dog', 'kat']),
                   'q2': ([FINAL_STATE], ['z'])}, self.feature_table)

        print(hmm.get_emissions('q1'))

    def test_get_next_state(self):

        print('\nGET NEXT STATE')
        hmm = HMM({INITIAL_STATE: ['q1'],
                   'q1': (['q2', FINAL_STATE], ['dog', 'kat']),
                   'q2': ([FINAL_STATE], ['z'])}, self.feature_table)

        print(hmm._get_next_state())

    def test_draw(self):

        print('\nDRAW')
        hmm = HMM({INITIAL_STATE: ['q1'],
                   'q1': (['q2', FINAL_STATE], ['dog', 'kat']),
                   'q2': ([FINAL_STATE], ['z'])}, self.feature_table)

        print(hmm.draw())

    def test_combine_emissions(self):

        print('\nCOMBINE EMISSIONS')
        hmm = HMM({INITIAL_STATE: ['q1'],
                   'q1': (['q2', FINAL_STATE], ['dog', 'kat']),
                   'q2': ([FINAL_STATE], ['z'])}, self.feature_table)

        print('before\n', hmm)

        print('after\n',hmm.combine_emissions(), '\n', hmm)

    def test_clone_state(self):

        print('\nCLONE STATE')
        hmm = HMM({INITIAL_STATE: ['q1'],
                   'q1': (['q2', FINAL_STATE], ['dog', 'kat']),
                   'q2': ([FINAL_STATE], ['z'])}, self.feature_table)

        print('before\n', hmm)

        print('after\n', hmm.clone_state(), '\n', hmm)

    def test_clone_emission(self):

        print('\nCLONE EMISSION')
        hmm = HMM({INITIAL_STATE: ['q1'],
                   'q1': (['q2', FINAL_STATE], ['dog', 'kat']),
                   'q2': ([FINAL_STATE], ['z'])}, self.feature_table)

        print('before\n', hmm)

        print('after\n', hmm.clone_emission(), '\n', hmm)

    def test_add_state(self):

        print('\nADD STATE')
        hmm = HMM({INITIAL_STATE: ['q1'],
                   'q1': (['q2', FINAL_STATE], ['dog', 'kat']),
                   'q2': ([FINAL_STATE], ['z'])}, self.feature_table)

        print('before\n', hmm)

        print('after\n', hmm.add_state(), '\n', hmm)

    def test_remove_state(self):

        print('\nREMOVE STATE')
        hmm = HMM({INITIAL_STATE: ['q1'],
                   'q1': (['q2', FINAL_STATE], ['dog', 'kat']),
                   'q2': ([FINAL_STATE], ['z'])}, self.feature_table)

        print('before\n', hmm)

        print('after\n', hmm.remove_state(), '\n', hmm)

    def test_add_transition(self):

        print('\nADD TRANSITION')
        hmm = HMM({INITIAL_STATE: ['q1'],
                   'q1': (['q2', FINAL_STATE], ['dog', 'kat']),
                   'q2': ([FINAL_STATE], ['z'])}, self.feature_table)

        print('before\n', hmm)

        print('after\n', hmm.add_transition(), '\n', hmm)

    def test_remove_transition(self):

        print('\nREMOVE TRANSITION')
        hmm = HMM({INITIAL_STATE: ['q1'],
                   'q1': (['q2', FINAL_STATE], ['dog', 'kat']),
                   'q2': ([FINAL_STATE], ['z'])}, self.feature_table)

        print('before\n', hmm)

        print('after\n', hmm.remove_transition(), '\n', hmm)

    def test_add_segment_to_emission(self):

        print('\nADD SEGMENT TO EMISSION')
        self.feature_table = FeatureTable.load(get_feature_table_fixture("plural_english_feature_table.json"))
        hmm = HMM({INITIAL_STATE: ['q1'],
                   'q1': (['q2', FINAL_STATE], ['dog', 'kat']),
                   'q2': ([FINAL_STATE], ['z'])}, self.feature_table)

        segments = [segment.get_symbol() for segment in self.feature_table.get_segments()]
        print('segments', segments)
        print(hmm.add_segment_to_emission(segments), '\n', hmm)

    def test_remove_segment_from_emission(self):

        print('\nREMOVE SEGMENT FROM EMISSION')
        self.feature_table = FeatureTable.load(get_feature_table_fixture("plural_english_feature_table.json"))
        hmm = HMM({INITIAL_STATE: ['q1'],
                   'q1': (['q2', FINAL_STATE], ['dog', 'kat']),
                   'q2': ([FINAL_STATE], ['z'])}, self.feature_table)

        segments = [segment.get_symbol() for segment in self.feature_table.get_segments()]
        print('segments', segments)
        print(hmm.remove_segment_from_emission(), '\n', hmm)

    def test_change_segment_in_emission(self):

        print('\nCHANGE SEGMENT IN EMISSION')
        self.feature_table = FeatureTable.load(get_feature_table_fixture("plural_english_feature_table.json"))
        hmm = HMM({INITIAL_STATE: ['q1'],
                   'q1': (['q2', FINAL_STATE], ['dog', 'kat']),
                   'q2': ([FINAL_STATE], ['z'])}, self.feature_table)

        segments = [segment.get_symbol() for segment in self.feature_table.get_segments()]
        print('segments', segments)
        print('before\n', hmm)
        print(hmm.change_segment_in_emission(segments), '\n', hmm)

    def test_add_emission_to_state(self):

        print('\nADD EMISSION TO STATE')
        self.feature_table = FeatureTable.load(get_feature_table_fixture("plural_english_feature_table.json"))
        hmm = HMM({INITIAL_STATE: ['q1'],
                   'q1': (['q2', FINAL_STATE], ['dog', 'kat']),
                   'q2': ([FINAL_STATE], ['z'])}, self.feature_table)

        segments = [segment.get_symbol() for segment in self.feature_table.get_segments()]
        print('segments', segments)
        print('before\n', hmm)
        print(hmm.add_emission_to_state(segments), '\n', hmm)

    def test_remove_emission_from_state(self):

        print('\nREMOVE EMISSION FROM STATE')

        hmm = HMM({INITIAL_STATE: ['q1'],
                   'q1': (['q2', FINAL_STATE], ['dog', 'kat']),
                   'q2': ([FINAL_STATE], ['z'])},self.feature_table)

        print('before\n', hmm)
        print(hmm.remove_emission_from_state(), '\n', hmm)

    def test_underspecify(self):

        print('\nUNDERSPECIFY')
        hmm = HMM({INITIAL_STATE: ['q1'],
                   'q1': (['q2', FINAL_STATE], ['dog', 'kat']),
                   'q2': ([FINAL_STATE], ['z'])}, self.feature_table)

        print('before\n', hmm)
        print(hmm.underspecify(), '\n', hmm)

    def test_generate_emission(self):

        print('\nGENERATE EMISSION')
        hmm = HMM({INITIAL_STATE: ['q1'],
                   'q1': (['q2', FINAL_STATE], ['dog', 'kat']),
                   'q2': ([FINAL_STATE], ['z'])}, self.feature_table)

        print('generated: ',hmm.generate_emission())

    def test_generate_emissions_list(self):

        print('\nGENERATE EMISSION LIST')
        hmm = HMM({INITIAL_STATE: ['q1'],
                   'q1': (['q2', FINAL_STATE], ['dog', 'kat']),
                   'q2': ([FINAL_STATE], ['z'])}, self.feature_table)

        print('generated: ', hmm.generate_emissions_list(10))

    def test_make_mutation(self):

        print('\nMAKE MUTATION')
        self.feature_table = FeatureTable.load(get_feature_table_fixture("plural_english_feature_table.json"))
        hmm = HMM({INITIAL_STATE: ['q1'],
                   'q1': (['q2', FINAL_STATE], ['dog', 'kat']),
                   'q2': ([FINAL_STATE], ['z'])}, self.feature_table)
        print('before', hmm)

        print('after', hmm.make_mutation(), hmm)

    def test_get_transducer(self):
        print('\nGET TRANSDUCER')
        self.feature_table = FeatureTable.load(get_feature_table_fixture("plural_english_feature_table.json"))
        hmm = HMM({INITIAL_STATE: ['q0'],
                   'q0': (['q1', FINAL_STATE], ['dog', 'kat']),
                   'q1':([FINAL_STATE], ['z'])
                   }, self.feature_table)
        '''hmm2 = HMM({INITIAL_STATE: ['q0'],
                   'q0': (
                   [FINAL_STATE], ['t']),
                   }, self.feature_table)'''
        z=hmm.get_transducer()
        #print(hmm.get_transducer())

        print('list of transducer states', hmm.get_list_of_transducer_states())
        #print ('PRINT', hmm)
        #print ('get states', z.get_states())

        #print("get alph", z.get_alphabet())
        #print("get final", z.get_final_states())
        #print("get arcs", z.get_arcs())
        print(z)
        for i in z.get_arcs():
            print(type(i.input))
        #for i in z.get_arcs():
            #print (i.origin_state.index)
        #print('dot', z.dot_representation)
        write_to_dot(z, 'hmm_kat')
        hmm.draw()

    def test_morpheme_boundary(self):

         self.feature_table = FeatureTable.load(get_feature_table_fixture("plural_english_feature_table.json"))
         hmm = HMM({INITIAL_STATE: ['q1'],
                     'q1': (['q2', FINAL_STATE], ['dog', 'kat']),
                     'q2': ([FINAL_STATE], ['z'])}, self.feature_table)

         morpheme_boundary_hmm_transducer = hmm.get_transducer()
         write_to_dot(morpheme_boundary_hmm_transducer, "morpheme_boundary_hmm_transducer")






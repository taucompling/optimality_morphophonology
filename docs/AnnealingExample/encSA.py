from random import choice
class Perm:
    def __init__(self, mapping):
        self.mapping = mapping

    def get_neighbor(self):
        """Get a random neighbor of the current permutation."""
        new_perm = Perm(self.mapping.copy())
        map_keys = list(new_perm.mapping.keys())

        # Choose two random keys and switch them
        first_key = choice(map_keys)
        second_key = choice(map_keys)

        temp = new_perm.mapping[first_key]
        print('first key', temp)
        new_perm.mapping[first_key] = new_perm.mapping[second_key]
        print('new first key',new_perm.mapping[first_key])
        new_perm.mapping[second_key] = temp
        print('new second_key',temp)
        return new_perm

    def translate(self, string):
        """Translate a given string according to the current permutation."""
        translated_string = ''
        for i in range(len(string)):
            translated_string += self.mapping[string[i]]

        print ( translated_string)
    def get_mapping(self):
        """Get the current mapping."""
        return self.mapping
sigma='abcdefg'       
a=Perm({char:char for char in sigma})
a.get_neighbor()
a.translate('caca')
print (a.get_mapping())
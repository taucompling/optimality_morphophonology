
import logging
from io import StringIO
from grammar.feature_table import FeatureTable, MORPHEME_BOUNDARY, Segment
from transducer import Transducer, Arc, CostVector, State
from otml_configuration_manager import OtmlConfigurationManager, OtmlConfigurationError
import re


logger = logging.getLogger(__name__)
configurations = OtmlConfigurationManager.get_instance()
# if configurations is None:
#     raise OtmlConfigurationError("OtmlConfigurationManager was not initialized")

NULL_SEGMENT = "-"

class ParsingNFA:
    def __init__(self):
        self.initial_state = None
        self.final_states = None
        self.arcs_dict = None
        self.probabilities = None


    @classmethod
    def get_from_grammar_for_nfa_transducer(cls, feature_table, transducer):
        segments = feature_table.get_segments()
        transducer_ft = Transducer(segments, name="HMM", length_of_cost_vectors=0)
        nfa = ParsingNFA()
        nfa.final_states = list()
        arcs_dict = dict()
        probabilities = dict()
        for state in transducer.get_states():
            m = re.match('.*q(\w*).*', str(state))  # get sate number from the string: "q0"
            nfa_state1 = m.group(1)
            if state == transducer.initial_state:
                nfa.initial_state = nfa_state1
            if state in transducer.final_states:
                nfa.final_states.append(nfa_state1)
            arc=transducer.get_arcs_by_origin_state(state) #returns arcs
            nfa_state2 = arc.terminal_state
            output_symbol = Segment(arc.output).get_symbol()
            if output_symbol == u"\u03b5":
                output_symbol = NULL_SEGMENT
            if nfa_state1 not in arcs_dict:
                arcs_dict[nfa_state1] = {}
                probabilities[nfa_state1] = []
            if output_symbol not in arcs_dict[nfa_state1]:
                arcs_dict[nfa_state1][output_symbol] = []

            arcs_dict[nfa_state1][output_symbol].append(nfa_state2)
            probabilities[nfa_state1].append((output_symbol, nfa_state2))

        nfa.arcs_dict = arcs_dict
        nfa.probabilities = probabilities
        return nfa

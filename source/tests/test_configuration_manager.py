
import os
import codecs
from configuration_manager import ConfigurationManager,ConfigurationManagerError

dirname, filename = os.path.split(os.path.abspath(__file__))
json_str = codecs.open(os.path.join(dirname, "fixtures/configuration/configuration_example.json"), 'r').read()
configuration_manager = ConfigurationManager(json_str)
update_json_str = codecs.open(os.path.join(dirname, "fixtures/configuration/configuration_update.json"), 'r').read()
illegal_update_json_str = codecs.open(os.path.join(dirname, "fixtures/configuration/illegal_configuration_update.json"), 'r').read()


class TestConfigurationManager():
       
    def test_singeltoness(self):
        assert id(configuration_manager) == id(ConfigurationManager())

    def test_configurations(self):
        assert configuration_manager["PROPERTY1"] == "spam"

    def test_update_configuration(self):
        configuration_manager.update_configurations(update_json_str)
        assert configuration_manager["PROPERTY1"] == "egg"

    '''def test_update_configuration_illegal(self):
        with self.assertRaises(ConfigurationManagerError):
            configuration_manager.update_configurations(self.illegal_update_json_str)'''


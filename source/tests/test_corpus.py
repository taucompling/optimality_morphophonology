from tests.otml_configuration_for_testing import configurations
from corpus import Corpus
from grammar.feature_table import FeatureTable
from tests.persistence_tools import get_corpus_fixture, get_feature_table_fixture

feature_table = FeatureTable.load(get_feature_table_fixture("feature_table.json"))
corpus = Corpus.load(get_corpus_fixture("corpus.txt"))


class TestCorpus():

    def test_number_of_words(self):
        assert len(corpus) == 82

    def test_get_item(self):
        assert corpus[3] == "bab"
        assert corpus[5] == "aab"


    def test_get_list_corpus(self):
        corpus = Corpus.load(get_corpus_fixture("test_list_corpus.txt"))
        assert len(corpus) == 5




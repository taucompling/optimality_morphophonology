from math import exp, log
from random import random, choice
from time import time
from copy import copy
from functools import reduce

# Simulated annealing example

# Decodes a text that was encrypted by a permutation of the set of English alphabet and some punctuation marks.
# Objective: finding the permutation that maximizes the probability of the text.
# Probability model: a bigram model computed over an English training corpus.


trainingCorpusFilePath = 'Gutenberg.txt'
inputFilePath = 'input.txt'

# Initialization
whitelist = ',. :\n#()\'\"!?'
words = open(trainingCorpusFilePath).read()
words = ''.join(c for c in words if c.isalpha() or c in whitelist).lower()

symbols = set(words)
symbolsNum = len(symbols)
textLength = len(words)

# Compute unigrams and bigrams

unigram_counts = {symbol: 0 for symbol in symbols}
bigram_counts = {symbol1: {symbol2: 0  for symbol2 in symbols} for symbol1 in symbols}

for i in range(len(words) - 1):
    unigram_counts[words[i]] += 1
    bigram_counts[words[i]][words[i+1]] += 1

unigram_counts[words[-1]] += 1

unigrams = {symbol: unigram_counts[symbol] / textLength for symbol in symbols}

def computeBigram(symbol1, symbol2): 
    return (bigram_counts[symbol1][symbol2] + 1) / (unigram_counts[symbol1] + symbolsNum)

bigrams = {symbol1: {symbol2: computeBigram(symbol1,symbol2) 
                     for symbol2 in symbols} for symbol1 in symbols}

def getUnigram(symbol1):
    return unigrams[symbol1]
    
def getBigram(symbol1, symbol2):
    return bigrams[symbol1][symbol2]

# Permutation Class

class Perm:
    def __init__(self,mapping):
        self.mapping = mapping

    def getNeighbor(self):
        newPerm = Perm(copy(self.mapping))   #deepcopy(self)
        chars = list(newPerm.mapping.keys())
        a = choice(chars)
        b = choice(chars)        
        newPerm.mapping[a], newPerm.mapping[b] = newPerm.mapping[b], newPerm.mapping[a]
        return newPerm

    def translate(self,string):
        return ''.join(self.mapping[string[i]] for i in range(len(string)))
    
    def __str__(self): return str(self.mapping)

# Energy
        
def getEnergy(H,D):
    decoded = H.translate(D)
    energy = -log(getUnigram(decoded[0]), 2)
    for i in range(1, len(decoded)-1):
        energy += -log(getBigram(decoded[i], decoded[i+1]) ,2)
    return energy

# Simulated Annealing

def runAnnealing(initialHypothesis, data):
    
    T = 10**12
    threshold = 10**-7
    cooling = 0.9995

    H = initialHypothesis
    D = data
    
    energy = getEnergy(H,D)
    
    while T > threshold:
        
        print('Energy = ',energy)
        print('Temperature = ', T)
        print(H)
        
        H1 = H.get_neighbor()
        newEnergy = getEnergy(H1, D)
        delta = newEnergy - energy
        
        if delta < 0:
            p = 1
        else:
            p = exp(-delta / T)
        if random() < p:
            H = H1
            energy = newEnergy
        T *= cooling
    
    return H

data = open(inputFilePath).read()
initialHypothesis = Perm({symbol:symbol for symbol in symbols})

H = runAnnealing(initialHypothesis, data)

result = H.translate(data)

print(result)
print(H)

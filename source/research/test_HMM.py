from tests.otml_configuration_for_testing import configurations
from debug_tools import write_to_dot, write_to_dot_hmm
from grammar.feature_table import FeatureTable
from research.HMM import HMM, INITIAL_STATE, FINAL_STATE
from tests.persistence_tools import get_feature_table_fixture

feature_table = FeatureTable.load(get_feature_table_fixture("plural_english_feature_table.json"))
hmm = HMM({INITIAL_STATE: ['q1'],
                   'q1': (['q2', FINAL_STATE], ['dog', 'kat']),
                   'q2': ([FINAL_STATE], ['z'])
                   }, feature_table)
hmm_transducer = hmm.get_transducer()
write_to_dot(hmm_transducer, 'hmm_transducer_kat')
write_to_dot_hmm(hmm, 'HMM_plural_cat_dog')
print(hmm.draw())


import os
from tests.otml_configuration_for_testing import configurations
from grammar.lexicon import Word
from grammar.feature_table import FeatureTable
from grammar.constraint_set import ConstraintSet
from grammar.lexicon import Lexicon
from grammar.grammar import Grammar
from corpus import Corpus
from traversable_grammar_hypothesis import TraversableGrammarHypothesis
from tests.persistence_tools import get_constraint_set_fixture, get_feature_table_fixture, get_corpus_fixture

feature_table = FeatureTable.load(get_feature_table_fixture("a_b_and_son_feature_table.json"))
constraint_set = ConstraintSet.load(get_constraint_set_fixture("no_bb_Max_Dep_constraint_set.json"),
                                                  feature_table)
corpus = Corpus.load(get_corpus_fixture("testing_parser_suite_corpus.txt"))
lexicon = Lexicon(corpus.get_words(),feature_table)
grammar = Grammar(feature_table, constraint_set, lexicon)
bb = Word("bb", feature_table)
bab = Word("bab", feature_table)
abba = Word("abba", feature_table)
ababa = Word("ababa", feature_table)

dirname, filename = os.path.split(os.path.abspath(__file__))

class TestingParserSuite():
    """ this test case is designed test the parser and related function: get_range and generate
    see: https://taucompling.atlassian.net/wiki/display/OTML/Testing+parser+suite
    """
    def test_generate(self):
        assert grammar.generate(bb) == {"bab"}
        assert grammar.generate(bab) == {"bab"}

        assert grammar.generate(abba) == {"ababa"}
        assert grammar.generate(ababa) == {"ababa"}


    def test_parser(self):
        traversable_hypothesis = TraversableGrammarHypothesis(grammar, ["bb"])
        assert traversable_hypothesis.parse_data() == {'bb': set()}

        traversable_hypothesis = TraversableGrammarHypothesis(grammar, ["ababa"])
        assert traversable_hypothesis.parse_data() == {"ababa": set([(abba, 1), (ababa, 1)])}

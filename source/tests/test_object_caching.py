from tests.otml_configuration_for_testing import configurations
from grammar.feature_table import FeatureTable
from grammar.lexicon import Word
from grammar.lexicon import Lexicon
from corpus import Corpus
from grammar.constraint import MaxConstraint, PhonotacticConstraint
from grammar.constraint_set import ConstraintSet
from grammar.grammar import Grammar
from tests.persistence_tools import get_constraint_set_fixture, get_feature_table_fixture, get_corpus_fixture
from debug_tools import timeit
import pickle

feature_table = FeatureTable.load(get_feature_table_fixture("a_b_and_son_feature_table.json"))
constraint_set_filename = get_constraint_set_fixture("no_bb_Max_Dep_constraint_set.json")
corpus = Corpus.load(get_corpus_fixture("small_ab_corpus.txt"))
word = Word("abababa",feature_table)
constraint = PhonotacticConstraint([{'son': '+'}, {'son': '+'}], feature_table)
constraint_set = ConstraintSet.load(constraint_set_filename, feature_table)
lexicon = Lexicon(corpus.get_words(), feature_table)
grammar = Grammar(feature_table, constraint_set, lexicon)

class TestObjectCaching():
        
    def test_constraint_transducer_caching(self):
        max_constraint = MaxConstraint([{'son': '+'}], feature_table)
        #deepcopy(max_constraint)
        orig_transducer = max_constraint.get_transducer()
        max_constraint.augment_feature_bundle()
        new_transducer = max_constraint.get_transducer()
        assert id(orig_transducer) == id(new_transducer)

    #def test_word_caching(self):
    #    get_transducer(word)
    #    get_transducer(word)
    #    get_transducer(word)
    #
    #def test_constraint_caching(self):
    #    get_transducer(constraint)
    #    get_transducer(constraint)
    #    get_transducer(constraint)
    #
    #
    #def test_constraint_set_caching(self):
    #    get_transducer(constraint_set)
    #    get_transducer(constraint_set)
    #    get_transducer(constraint_set)
    #
    #def test_grammar_caching(self):
    #    get_transducer(grammar)
    #    get_transducer(grammar)
    #    get_transducer(grammar)

    def test_generate_caching(self):
        word = Word("bbb", feature_table)
        word_outputs = grammar.generate(word)
        from grammar.grammar import outputs_by_constraint_set_and_word

        constraint_set_and_word_key = str(grammar.constraint_set) + str(word)

        assert set(outputs_by_constraint_set_and_word[constraint_set_and_word_key]) == set(word_outputs)

    def test_parse_data_caching(self):
        pass


    def test_start_from_middle(self):
        print()
        print(pickle.dumps(configurations))

@timeit
def get_transducer(o):
    return o.get_transducer()





import re

from grammar.constraint_set import ConstraintSet
from grammar.feature_table import FeatureTable
from grammar.grammar import Grammar
from research.HMM import HMM
from tests.persistence_tools import get_constraint_set_fixture, get_feature_table_fixture
from tests.stochastic_testcase import StochasticTestCase


class TestParsingNFA(StochasticTestCase):

    def setUp(self):
        INITIAL_STATE = 'qH0'
        FINAL_STATE = 'qHf'
        ft = FeatureTable.load(get_feature_table_fixture("french_deletion_feature_table.json"))
        lexicon = HMM({INITIAL_STATE: ['q0'],
                       'q0': ([FINAL_STATE],
                              ['tabl', 'libl', 'tapl', 'ridl', 'parl', 'birl', 'tpal', 'tbir', 'rdahl', 'tahl']),
                       }, ft)
        print(lexicon)
        constraint_set = ConstraintSet.load(get_constraint_set_fixture("french_deletion_constraint_set.json"), ft)
        self.grammar = Grammar(ft, constraint_set, lexicon)


    def test_get_nfa(self):
        c=self.grammar.get_transducer()
        print(c)
        print (c.get_states())
        for state in c.get_states():
            m = re.match('.*q(\w*).*', str(state))
            nfa_state1 = m.group(1)
            print('nfa1',nfa_state1)
            for arc in c.get_arcs():
                c.get_arcs_by_origin_state(state)
                nfa_state2 = arc.terminal_state
                #print('two',nfa_state2)

            #arc = c.get_arcs_by_origin_state(state)
            #print (arc)
            #print(type(arc))
            #nfa_state2 = Arc (arc).terminal_state
            #print (nfa_state2)
            '''print('state',state)
print('str', str(state))
m = re.match('.*q(\w*).*', str(state))  # get sate number from the string: "q0"

print (m)
nfa_state1 = m.group(1)
print ('nfa state',nfa_state1)'''





        #c=self.grammar.get_nfa()
       # print (c)






import codecs


from tests.otml_configuration_for_testing import configurations
from grammar.feature_table import FeatureTable, FeatureParseError, Segment, FeatureType
from grammar.feature_bundle import FeatureBundle
from tests.stochastic_testcase import StochasticTestCase
from tests.persistence_tools import get_feature_table_fixture, get_feature_table_by_fixture


class TestFeatureTable(StochasticTestCase):

    def setUp(self):
        self.correct_set_filename = get_feature_table_fixture("feature_table.json")
        self.illegal_feature_value_filename = get_feature_table_fixture("illegal_feature_table_illegal_"
                                                                    "feature_value.json")
        self.mismatch_in_number_of_features_filename = get_feature_table_fixture("illegal_feature_table_"
                                                                             "mismatch_in_number_of_features.json")
        self.feature_table = FeatureTable.load(self.correct_set_filename)

    def test_get_item_with_feature_label(self):
        self.assertEqual(self.feature_table['a', "syll"], '+')
        self.assertEqual(self.feature_table['b', "syll"], '-')
        self.assertEqual(self.feature_table['c', "syll"], '+')
        self.assertEqual(self.feature_table['d', "syll"], '-')
        self.assertEqual(self.feature_table['a', "son"], '-')
        self.assertEqual(self.feature_table['b', "son"], '-')
        self.assertEqual(self.feature_table['c', "son"], '+')
        self.assertEqual(self.feature_table['d', "son"], '+')

    def test_get_item_invalid(self):
        with self.assertRaises(Exception):
            res = self.feature_table['a', 2]

        with self.assertRaises(Exception):
            res = self.feature_table['y', 5]

        with self.assertRaises(Exception):
            res = self.feature_table[5]


    def test_illegal_set_illegal_feature_value(self):
        with self.assertRaises(FeatureParseError):
            ft = FeatureTable.load(self.illegal_feature_value_filename)

    def test_illegal_set_illegal_mismatch_in_number_of_features(self):
        with self.assertRaises(FeatureParseError):
            ft = FeatureTable.load(self.mismatch_in_number_of_features_filename)

    def test_from_csv(self):
        feature_table = self.correct_set_filename = get_feature_table_by_fixture("a_b_and_son_feature_table.csv")
        self.assertEqual(feature_table['a', "cons"], '-')



    #segment tests:
    def test_segment(self):
        segment = Segment('a', self.feature_table)
        self.assertEqual(str(segment), "Segment a[+, -]")
        self.assertEqual(segment['son'], u'-')


    def test_segment_encoding_Length(self):
        segment = Segment('a', self.feature_table)
        print(self.assertEqual(segment.get_encoding_length(), 2))


    def test_segment_has_feature_bundle(self):
        segment = Segment('a', self.feature_table)
        self.assertTrue(segment.has_feature_bundle(FeatureBundle({'syll': '+'}, self.feature_table)))
        self.assertTrue(segment.has_feature_bundle(FeatureBundle({'son': '-'}, self.feature_table)))
        self.assertTrue(segment.has_feature_bundle(FeatureBundle({'syll': '+', 'son': '-'}, self.feature_table)))
        self.assertFalse(segment.has_feature_bundle(FeatureBundle({'son': '+'}, self.feature_table)))
        self.assertFalse(segment.has_feature_bundle(FeatureBundle({'syll': '+', 'son': '+'}, self.feature_table)))


    #featureType tests:
    def test_feature_type(self):
        feature = FeatureType('syll', ['+', '-'])
        self.assertEqual(str(feature), "FeatureType syll with possible values: [+, -]")
        self.assertEqual('+' in feature, True)
        self.assertEqual('?' in feature, False)

def main():
    feature_table_dict_from_json = {"feature": [{"label": "cons", "values": ["-", "+"]}],
                                    "feature_table": {"a": ["-"], "b": ["+"]}}


    a = FeatureTable(feature_table_dict_from_json)
    print (a)
    print("Feature table dict",a.feature_table_dict)
    feature_type_list = [dict(**feature) for feature in feature_table_dict_from_json['feature']]

    for item in feature_type_list:
        print ('feature type list', item)
        print ('full list', feature_type_list)
    for symbol in a.feature_table_dict:
        print('symbol', symbol)
    feature_types_label_in_order = [feature['label'] for feature in feature_table_dict_from_json['feature']]
    for f in feature_types_label_in_order:
        print('feature type', f)
        print('feature_types_label_in_order', feature_types_label_in_order)
    print("Feature types dict", a.feature_types_dict)
    print("Segment list", a.segments_list)
    b=Segment('b')
    c=[i for i in a.segments_list if i==b]
    print (c)

    print('BBBBBBBBBB',b)
    print( 'AAAAAAAAAAA',b in a.segments_list)
    print ('Feature order dict', a.feature_order_dict)
    for symbol in feature_table_dict_from_json['feature_table'].keys():
        feature_values = feature_table_dict_from_json['feature_table'][symbol]
        print ('symbol',symbol, 'fvalues',feature_values)
        print(len(feature_values))
        print (len(a.feature_types_dict))
        symbol_feature_dict = dict()
        for i, feature_value in enumerate(feature_values):
            print ('enum feat val', list(enumerate(feature_values)))
            feature_label = feature_types_label_in_order[i]
            print('feature label', feature_label)
            if not feature_value in a.feature_types_dict[feature_label]:
                raise FeatureParseError("Illegal feature was found for segment {0}".format(symbol))
            symbol_feature_dict[feature_label] = feature_value
            a.feature_table_dict[symbol] = symbol_feature_dict
            print('ft and fd',feature_value,symbol_feature_dict)
        for symbol in a.get_alphabet():
            a.segments_list.append(Segment(symbol))
            print ('alpha',a.get_alphabet())

    print ('symbol_feature_dict',symbol_feature_dict)
    print ('LOADS', a.loads( '{"feature": [{"label": "cons", "values": ["-", "+"]}],"feature_table": {"a": ["-"], "b": ["+"]}}'))
    print ('get no. of features',a.get_number_of_features())
    print ('get features', a.get_features())
    print ('get rand value', a.get_random_value('cons'))
    print ('get alphabet', a.get_alphabet())
    print ('get segments', a.get_segments())
    print ('get_random_segment', a.get_random_segment())
    print('get_ordered_feature_vector',a.get_ordered_feature_vector('a'))
    print('is_valid_feature', a.is_valid_feature('cons'))
    print('is_valid_feature', a.is_valid_feature('son'))

    feature_table= {"a": ["-"], "b": ["+"]}
    symbol1='a'
    symbol2='b'
    c=Segment(symbol1, feature_table)

    d=Segment(symbol2, feature_table)

    print(d==c)
    print('INTERSECT', c&d)
    print('get_encoding_length',c.get_encoding_length())
    print('get symbol', c.get_symbol())
    print (c.feature_dict)
    print(feature_table['a'])

    ft=FeatureType('cons',["-", "+"])
    print('get_random_value',ft.get_random_value())




main()
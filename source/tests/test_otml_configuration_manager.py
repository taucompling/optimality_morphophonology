import os
import codecs
from otml_configuration_manager import OtmlConfigurationManager,OtmlConfigurationError

dirname, filename = os.path.split(os.path.abspath(__file__))
json_str = codecs.open(os.path.join(dirname, "fixtures/configuration/otml_configuration.json"),'r').read()
otml_configuration_manager = OtmlConfigurationManager(json_str)

class TestOtmlConfigurationManager():
    
    def test_configurations(self):
        assert otml_configuration_manager["MIN_NUMBER_OF_CONSTRAINTS_IN_CONSTRAINT_SET"] == 1

    def test_mappings(self):
        assert otml_configuration_manager["MAX_FEATURES_IN_BUNDLE"] == float("inf")
        assert otml_configuration_manager["THRESHOLD"] == 10**-2

    def test_derived_configurations(self):
        assert otml_configuration_manager["LEXICON_SELECTION_WEIGHT"] == 2
        assert otml_configuration_manager["PHONOTACTIC_WEIGHT_FOR_INSERT"] == 1


    def test_update(self):
        otml_configuration_manager["MIN_NUMBER_OF_CONSTRAINTS_IN_CONSTRAINT_SET"] = 2
        assert otml_configuration_manager["MIN_NUMBER_OF_CONSTRAINTS_IN_CONSTRAINT_SET"] == 2
        otml_configuration_manager["LEXICON_MUTATION_WEIGHTS"] = {"insert_segment": 1,
                                                                       "delete_segment": 0,
                                                                       "change_segment": 0}
        assert otml_configuration_manager["LEXICON_SELECTION_WEIGHT"] == 1
        # test reset_to_original_configurations()
        otml_configuration_manager.reset_to_original_configurations()
        assert otml_configuration_manager["MIN_NUMBER_OF_CONSTRAINTS_IN_CONSTRAINT_SET"] == 1

    '''def test_validation(self):
        with assertRaises(OtmlConfigurationError):
            otml_configuration_manager["CONSTRAINT_INSERTION_WEIGHTS"] = {"Dep": 0,
                                                                               "Max": 0,
                                                                               "Ident": 0,
                                                                               "Phonotactic": 0}'''

    def tearDown(self):
        otml_configuration_manager.reset_to_original_configurations()

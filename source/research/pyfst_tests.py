import fst
import os
from tests.otml_configuration_for_testing import configurations
from os.path import join, split, abspath
from grammar.feature_table import FeatureTable
from tests.persistence_tools import get_feature_table_fixture
import codecs
from codecs import decode
import operator
import functools

#make a symbol table

symbol_table = fst.SymbolTable()
word = 'cat'
for letter in word:
    symbol_table[letter]


max_constraint = fst.Transducer(isyms=symbol_table, osyms=symbol_table)
for index, symbol in enumerate(word):
    max_constraint.add_arc(index, index, symbol, symbol, 0)
    max_constraint.add_arc(index, index, symbol, NULL_SEGMENT, 1)
    max_constraint.add_arc(index, index, NULL_SEGMENT, symbol, 0)
max_constraint.arc_sort_input()

print('MAX CONSTRAINT \n')
for state in max_constraint.states:
    for arc in state.arcs:
        print('{} -> {} / {}:{} / {}'.format(state.stateid,
                                             arc.nextstate,
                                             max_constraint.isyms.find(arc.ilabel),
                                             max_constraint.osyms.find(arc.olabel),
                                             arc.weight))

print('max', max_constraint)

dep_constraint = fst.Transducer(isyms=symbol_table, osyms=symbol_table)
for index, symbol in enumerate(word):
    dep_constraint.add_arc(index, index, symbol, symbol, 0)
    dep_constraint.add_arc(index, index, symbol, NULL_SEGMENT, 0)
    dep_constraint.add_arc(index, index, NULL_SEGMENT, symbol, 1)
dep_constraint.arc_sort_input()
print('DEP CONSTRAINT \n')
for state in dep_constraint.states:
    for arc in state.arcs:
        print('{} -> {} / {}:{} / {}'.format(state.stateid,
                                             arc.nextstate,
                                             dep_constraint.isyms.find(arc.ilabel),
                                             dep_constraint.osyms.find(arc.olabel),
                                             arc.weight))

print('dep', dep_constraint)

composed_max_dep = max_constraint >> dep_constraint
composed_max_dep.remove_epsilon()
for state in composed_max_dep.states:
    for arc in state.arcs:
        print('{} -> {} / {}:{} / {}'.format(state.stateid,
                                             arc.nextstate,
                                             composed_max_dep.isyms.find(arc.ilabel),
                                             composed_max_dep.osyms.find(arc.olabel),
                                             arc.weight))
print('compose', composed_max_dep)







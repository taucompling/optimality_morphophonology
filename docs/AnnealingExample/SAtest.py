from math import exp, log
from random import random, choice
from time import time
from copy import copy
from functools import reduce

trainingText = 'Gutenberg.txt'
finput = 'inputmini.txt'

excludechars = ',. :\n#()\'\"!?'
words=open(trainingText).read()
#get rid of numbers and excludechars characters
words = ''.join(w for w in words if w.isalpha() or w in excludechars).lower()
#make a set of symbols without duplicates
symbols = set(words)
symbolsNum = len(symbols)
textLength = len(words)


#compute unigrams and bigrams

#1. set up a skeleton for bigrams and unigrams
unigram_counts = {symbol:0 for symbol in symbols}

bigram_counts = {symbol1: {symbol2:0 for symbol2 in symbols} for symbol1 in symbols}


#2

for i in range(len(words)-1):
    #count unigrams i
    unigram_counts[words[i]] +=1
    #count bigrams i+1
    bigram_counts[words[i]][words[i+1]] +=1

#count last char in words
unigram_counts[words[-1]] += 1
#calculate probabilities for unigrams
unigrams = {symbol: unigram_counts[symbol] / textLength for symbol in symbols}


#calculate prob for bigrams
def computeBigram(symbol1, symbol2):
    return (bigram_counts[symbol1][symbol2] + 1) / (unigram_counts[symbol1] + symbolsNum)

bigrams = {symbol1: {symbol2: computeBigram(symbol1,symbol2)
                     for symbol2 in symbols} for symbol1 in symbols}
print (bigrams)

#getters
def getUnigram(symbol1):
    return unigrams[symbol1]
def getBigram(symbol1, symbol2):
    return bigrams[symbol1][symbol2]

#mutations
class Mutation:
    def __init__(self, mapping):
        self.mapping = mapping

    def getNeighbor(self):
        #define new mutation
        newMutation = Mutation(copy(self.mapping))  # deepcopy(self)
        #get chars from mapping dict
        chars = list(newMutation.mapping.keys())
        a = choice(chars)
        b = choice(chars)
        #switch places between chars
        newMutation.mapping[a], newMutation.mapping[b] = newMutation.mapping[b], newMutation.mapping[a]
        return newMutation
    #redefine translate
    def translate(self, string):
        return ''.join(self.mapping[string[i]] for i in range(len(string)))

    #redefine str repr
    def __str__(self): return str(self.mapping)

    # Energy


def getEnergy(H, D):
    decoded = H.translate(D)
    #cost function
    energy = -log(getUnigram(decoded[0]), 2)
    for i in range(1, len(decoded) - 1):
        energy += -log(getBigram(decoded[i], decoded[i + 1]), 2)
    return energy


# Simulated Annealing

def runAnnealing(initialHypothesis, data):
    T = 10 ** 12
    threshold = 10 ** -7
    cooling = 0.9995

    H = initialHypothesis
    D = data

    energy = getEnergy(H, D)

    while T > threshold:

        print('Energy = ', energy)
        print('Temperature = ', T)
        print(H)

        H1 = H.getNeighbor()
        newEnergy = getEnergy(H1, D)
        delta = newEnergy - energy

        if delta < 0:
            p = 1
        else:
            p = exp(-delta / T)
        if random() < p:
            H = H1
            energy = newEnergy
        T *= cooling

    return H


data = open(finput).read()
initialHypothesis = Mutation({symbol: symbol for symbol in symbols})

H = runAnnealing(initialHypothesis, data)

result = H.translate(data)

print(result)
print(H)
